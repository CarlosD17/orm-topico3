from abc import ABC
from DB import DataBase

class AbstractModel(ABC):
    def __init__(self):
        self.db = DataBase()
    def getModel(self, *args):pass
    def select(self, *args):pass
    def insert(self, *args):pass
    def update(self, *args):pass
    def delete(self, *args):pass

class Users(AbstractModel):
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance

    def getModel(self, nombre):
         return nombre
    def select(self, attr, table, where = '', orderBy=''):
        return self.db.select(attr, table, where, orderBy)
    def insert(self, data, table):
        return self.db.insert(data, table)
    def update(self, data, table, where):
        return self.db.update(data, table, where)
    def delete(self, table, where):
        return self.db.delete(table, where)

    def login(self, email, password):
        result=""
        data={}
        user = self.select("id, nombre, apellidos, tipo", "usuarios", "email='"+email+"' AND password='"+password+"'")
        if user[1] > 0:
            user_data = {"id":user[0][0][0], "nombre": user[0][0][1], "apellidos":user[0][0][2], "tipo":user[0][0][3]}
            modules = self.getModules(str(user[0][0][0]))
            data["user"]=user_data
            data["modules"] = modules
            result = data
        else:
            result = "Credenciales invalidas"
        return result

    def register(self, data):
        result = ""
        datos = self.select("*", "usuarios", "email='"+data["email"]+"'")
        if datos[1] > 0:
            result = "Ya existe un usuario con ese correo"
        else:
            result = self.insert(data, "usuarios")
            user = self.select("*", "usuarios", "email='"+data["email"]+"'")
            result ={"id":user[0][0][0], "nombre": user[0][0][1], "apellidos":user[0][0][2], "email":user[0][0][3]}
        return result


    def getModules(self, user):
        data = []
        modules_id = self.select('modulos_id', 'accesos_menu','usuario_id='+user)
        for value in modules_id[0]:
            module = self.select("*", "modulos", "id="+str(value[0]))
            data.append( {"id":module[0][0][0],"name":module[0][0][1], "icon":module[0][0][2], "path":module[0][0][3]} )
        return data

    def updatePermissions_aux(self, user, catalog, permissions):
        for value in permissions:
            self.updatePermissions(user,catalog,str(value))
        return "Actulizado"

    def updatePermissions(self, user, catalog, permission):
        cate = self.select("*","asignacion_permisos", "usuario_id="+user+" AND catalogo_id="+catalog+" AND permiso_id="+permission)
        result =""
        if cate[1] > 0:
            result = self.delete("asignacion_permisos", "usuario_id="+user+" AND catalogo_id="+catalog+" AND permiso_id="+permission  )
            log={"accion":"Delete", "descripción":"Se elimino el permiso", "usuario_id":user}
            self.insert(log,'logs')
        else:
            data={'usuario_id':user, 'permiso_id':permission, 'catalogo_id':catalog}
            result = self.insert(data, 'asignacion_permisos')
            log={"accion":"Insert", "descripción":"Se inserto el permiso", "usuario_id":user}
            self.insert(log,'logs')
        return result



class Catalogos(AbstractModel):
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance
    def getModel(self, nombre):
         return nombre
    def select(self, attr, table, where = '', orderBy=''):
        return self.db.select(attr, table, where, orderBy)
    def insert(self, data, table):
        self.db.insert(data, table)
        da = self.select("*", table,"", "id desc limit 1")
        return da
    def update(self, data, table, where):
        return self.db.update(data, table, where)
    def delete(self, table, where):
        return self.db.delete(table, where)

    def getPermissions(self, user):
        data=[]
        per = []
        permissions_id = self.select('DISTINCT catalogo_id', 'asignacion_permisos', "usuario_id="+user)
        print(permissions_id)
        if permissions_id[0] is not None:
            for value in permissions_id[0]:
                permissions = self.select('permiso_id', 'asignacion_permisos', "usuario_id="+user+" AND catalogo_id="+str(value[0]))
                catalog = self.select("*", "catalogo", "id="+str(value[0]))
                for val in permissions[0]:
                    per.append(val[0])
                data.append({'tittle':catalog[0][0][1], "permissions":per})
                per=[]
        return data



    def getDataCatalog(self, catalog_name):
        dat={}
        data=[]
        aux={}
        data_meta={}
        data_fields=[]

        meta = self.select("COLUMN_NAME, COLUMN_TYPE", 'INFORMATION_SCHEMA.COLUMNS', "TABLE_NAME = '"+catalog_name+"' AND TABLE_SCHEMA='topico3_1'", "'ordinal_position'")
        data_table = self.select("*", catalog_name,"","id")

        for value in data_table[0]:
            aux={}
            for val in range(len(meta[0])):
                aux[meta[0][val][0]]=value[val]
            data.append(aux)

        for field in meta[0]:
            data_fields.append({"name":field[0],"dataType":field[1]})

        data_meta["entries"]=data_table[1]
        data_meta["fields"]=data_fields

        dat["meta"]=data_meta
        dat["data"]=data
        return dat

    def createCatalog(self, name, data):
        try:
            result = self.db.create(name,data)
            data={'nombre':name, 'estatus':'1'}
            new_catalog = self.insert(data, "catalogo")
            return "Catalogo creado correctamente"
        except Exception as e:
            print(e)
            return "Ya existe un catalogo con ese nombre"


    def dropCatalog(self,catalog_id, catalog_name, user_id):
        user = self.select('*', "asignacion_permisos", "usuario_id="+user_id+" AND catalogo_id="+catalog_id+" AND permiso_id=4")
        if user[1]>0:
            print("Eliminado")
        return


#clase abstracta
class AbstractFactory(ABC):
   def getUsers(self): pass
   def getCatalogos(self): pass


class ConcreteFactory(AbstractFactory):
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance
    def getUsers(self):
       return Users()
    def getCatalogos(self):
        return Catalogos()



# factory = ConcreteFactory()
# user = factory.getUsers()
# catalogos = factory.getCatalogos()
#
# print(user.login("carlosd_eg@yahoo.com.mx", "Contraseni"))
# print(catalogos.getDataCatalog("permisos"))
