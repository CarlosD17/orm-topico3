class Builder:
    instance=None
    def __new__(cls, *args, **kargs):
        if cls.instance is None:
            cls.instance=object.__new__(cls, *args, **kargs)
        return cls.instance

    def select(self, attr, table, where = '', orderBy=''):
        where = str("WHERE "+where) if (where != '' and where != None) else  ''
        orderBy = orderBy  if(orderBy != '' and  orderBy != None) else '1'
        statement = str("SELECT " + attr + " FROM " + table +" "+ where +" ORDER BY " + orderBy)
        return statement

    def insert(self, data, table):
        colums = ""
        values = ""
        for key in data:
            colums =colums + str(key + ",")
            values = values + str("'"+data[key]+"'"+",")
        colums = colums[:len(colums) - 1]
        values = values[:len(values) - 1]
        statement = str("INSERT INTO " + table +"("+colums+")"+ " VALUES("+values+")")
        return statement

    def update(self, data, table, where):
        values = ""
        for key  in data:
            values = str(key+"="+"'"+data[key]+"'"+",")
        values = values[:len(values) - 1]
        statement= str("UPDATE "+table+ " SET " + values + " WHERE " + where)
        return statement


    def delete(self, table, where):
        statement = str("DELETE FROM "+table+" WHERE "+where)
        return statement

    def create(self, name, data):
        values=""
        for key in data:
            values += str(key+" "+""+data[key]+""+",")
        values = values[:len(values) - 1]
        statement = str("CREATE TABLE "+name+" (" +values+",     PRIMARY KEY (id) )")
        print(statement)
        return statement

    def drop(self, name):
        statement = "DROP TABLE "+name
        return statement

# Bu = Builder()
#
# print(Bu.create("nueva_tabla", {"columna1":"varchar(200)", "columna2":"int(11)"}))
# print(Bu.drop("nueva_tabla"))
# print(orm.insert({"gg":"100", "dd":"101"}, "usuarios"))
# print(orm.update({"gg":"gg1", "dd":"dd1"}, "usuarios", "1=1"))
# print(orm.update({"gg":"gg1", "dd":"dd1"}, "usuarios", "1=1"))
